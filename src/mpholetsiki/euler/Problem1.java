package mpholetsiki.euler;

public class Problem1 {

	public static void main(String[] args) {
		long time = System.currentTimeMillis();
		System.out.println("Answer: " + getSumOfFactorsOfThreeAndFive(1000));
		System.out.println("Time completed: "
				+ (System.currentTimeMillis() - time) / 1000f + " seconds.");
	}

	public static int getSumOfFactorsOfThreeAndFive(int param) {

		int answer = 0;
		for (int i = 1; i < param; i++) {
			if (i % 3 == 0 || i % 5 == 0) {
				answer = answer + i;
			}
		}

		return answer;
	}
}
