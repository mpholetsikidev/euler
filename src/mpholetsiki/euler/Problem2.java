package mpholetsiki.euler;

public class Problem2 {

	public static void main(String[] args) {
		long time = System.currentTimeMillis();
		System.out.println("Answer: " + getSumOfFibonachiNumbers(4_000_000));
		System.out.println("Time completed: "
				+ (System.currentTimeMillis() - time) / 1000f + " seconds.");
	}

	public static int getSumOfFibonachiNumbers(int param) {
		
		int prevFirst = 0;
		int prevSecond = 1;
		int answer = 0;

		boolean exceed = false;
		while (!exceed) {
			int newFib = prevFirst + prevSecond;
			prevFirst = prevSecond;
			prevSecond = newFib;

			if (newFib > param) {
				exceed = true;
				break;
			}

			if (newFib % 2 == 0) {
				answer += newFib;
			}
		}

		return answer;
	}
}
