package mpholetsiki.euler;

public class Problem4 {

	public static void main(String[] args) {

		long time = System.currentTimeMillis();
		System.out.println("Answer: " + getLargestThreeDigitPalindrome());
		// System.out.println("Answer: " + getLargestTwoDigitPalindrome());
		System.out.println("Time completed: "
				+ (System.currentTimeMillis() - time) / 1000f + " seconds.");
	}

	public static int getLargestThreeDigitPalindrome() {
		int start = 100;
		int end = 999;
		int sum = 0;

		for (int i = end; i >= start; i--) {
			for (int j = end; j >= start; j--) {
				
				int product = i * j;
				String stringProduct = Integer.toString(product);
				String stringLastHalf = "";
				int stringMiddle = stringProduct.length()/2;
				
				for (int p = stringProduct.length()-1; p >= stringMiddle; p--) {
					stringLastHalf += stringProduct.charAt(p);		
				}

				if (stringProduct.startsWith(stringLastHalf) == true) {
					if (sum < product) {
						sum = product;
					}				
				}
			}
		}

		return sum;

	}

	// public static String getLargestTwoDigitPalindrome() {
	//
	// int answer = 0;
	//
	// for (int i = 1000; i > 0; i--) {
	// for (int j = 1000; j > 0; j--) {
	// answer = i * j;
	// String stringNum = Integer.toString(answer);
	//
	// if (stringNum.length() == 4)
	// {
	// String firstTwoChars =stringNum.charAt(0) + "" + stringNum.charAt(1);
	// String lastTwoChars = stringNum.charAt(3) + "" + stringNum.charAt(2);
	//
	// if (firstTwoChars.equals(lastTwoChars)) {
	// return stringNum;
	// }
	// }
	// }
	// }
	//
	// return null;
	// }
}
