package mpholetsiki.euler;

public class Problem3 {

	public static void main(String[] args) {
		long time = System.currentTimeMillis();
		System.out.println("Answer: " + getPrimeFactors(600851475143l));
		System.out.println("Time completed: "
				+ (System.currentTimeMillis() - time) / 1000f + " seconds.");
	}

	public static long getPrimeFactors(long param) {
		
		long answer = 0;
		int i = 2;

		while (i * i <= param) {
			if (param % i == 0) {
				param = param / i;
				answer = i;
			} else {
				i++;
			}
		}
		
		if (param > answer) {
			answer = param;
		}
		
		return answer;
	}
}
